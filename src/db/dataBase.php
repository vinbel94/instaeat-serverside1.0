<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 12:38
 */

require '../../../phpmongoDB/vendor/autoload.php';


class DataBase
{
    public $conn;
    public $db;

    /**
     * dataBase constructor.
     * @param $conn
     */
    public function __construct(){}


    public function getConnection(){

        $this->conn = null;

        try{
            $this->conn = new MongoDB\Client();
            $this->db = $this->conn->selectDatabase('instaeat');

        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->db;
    }
}