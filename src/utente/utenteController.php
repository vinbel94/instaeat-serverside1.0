<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/02/2019
 * Time: 13:57
 */
require_once("../utente/utente.php");
require_once("../utente/utenteCRUD.php");
require_once("../webServices/SimpleRest.php");


class utenteController extends SimpleRest
{
    public $utente;
    public $utenteCrud;
    public $result;

    /**
     * utenteController constructor.
     * @param $utente
     */
    public function __construct()
    {
        $this->utente = new utente();
        $this->utenteCrud = new utenteCRUD();
    }

    public function signUp($utente){


        // $this->utente = $this->utenteCrud->findUtente();
        if($this->chkEmail(json_decode($utente)->email) && $this->chkSizeEmail(json_decode($utente)->email)&& $this->checkPassword(json_decode($utente)->paswd) && $this->checkUsername(json_decode($utente)->nome)){


            $this->utente->setUsername(json_decode($utente)->nome);
            $this->utente->setPassword(json_decode($utente)->paswd);
            $this->utente->setEmail(json_decode($utente)->email);

            $queryPost = array('email' =>$this->utente->getEmail());

            $checkSinUp=$this->utenteCrud->findUtente($queryPost);


            if(empty($checkSinUp))
                $this->result = $this->utenteCrud->insertUtente($this->utente);
            else
                $this->result = false;

            //$this->error($this->result);
        }
        return $this->result;

    }


    public function logIn($utente){

        $checkLogin = [
            'id'=> '',
            'nome'=> '',
            'flag'=> false
        ];

        $queryPost = array();

        $this->utente->setUsername(json_decode($utente)->nome);
        $this->utente->setPassword(json_decode($utente)->paswd);
        $this->utente->setEmail(json_decode($utente)->email);

        $listUtenti = $this->utenteCrud->findUtente($queryPost);

        foreach($listUtenti as $user) {
            if (strcmp($this->utente->getEmail(), $user['email']) == 0 && strcmp($this->utente->getPassword(), $user['password']) == 0)
                $checkLogin = [
                    'id'=> $user['id'],
                    'nome'=> $user['name'],
                    'flag'=> true
                ];


        }


        // $this->error($listUtenti);

        return $checkLogin;
    }

    public function getUtenti($utente){

        $queryPost = array('_id' =>array('$ne'=> new \MongoDB\BSON\ObjectID(json_decode($utente))));

        $this->result = $this->utenteCrud->findUtente($queryPost);

        //$this->error($this->result);

        return $this->result;
    }

    public function getUtente($utente){
        $queryPost = array('_id' => new \MongoDB\BSON\ObjectID(json_decode($utente)));

        $this->result = $this->utenteCrud->findUtente($queryPost);

        //echo json_encode($this->result[0]['utentiSeguiti']);

        //$this->error($this->result);

        return $this->result;
    }

    function addFollow($params){

        if($this->checkAddFollow($params)) {
            $query = array('_id' => new \MongoDB\BSON\ObjectID(json_decode($params)->utenteLog->id));
            $update = array('$push' => array('utentiSeguiti' => array('idAutore' => new \MongoDB\BSON\ObjectID(json_decode($params)->utenteSearch->id))));

            $this->result = $this->utenteCrud->updateUtente($query, $update);

            //$this->error($this->result);

            if($this->result)
                return $this->getUtente(json_encode(json_decode($params)->utenteLog->id));
            else
                return $this->result;
        }
        else
            return false;

    }

    function removeFollow($params){

        $utente = $this->getUtente(json_encode(json_decode($params)->utenteSearch->id));

        if(!empty($utente)) {
            $query = array('_id' => new \MongoDB\BSON\ObjectID(json_decode($params)->utenteLog->id));
            $update = array('$pull' => array('utentiSeguiti' => array('idAutore' => new \MongoDB\BSON\ObjectID(json_decode($params)->utenteSearch->id))));

            $this->result = $this->utenteCrud->updateUtente($query, $update);

            //$this->error($this->result);

            if($this->result)
                return $this->getUtente(json_encode(json_decode($params)->utenteLog->id));
            else
                return $this->result;
        }
        else
            return false;
    }


    function error ($result){
        if(!$result) {
            $statusCode = 404;
            $result = array('error' => 'Not Found!');
            echo json_encode($result);
        }
        else {
            $statusCode = 200;
        }

        $requestContentType = $_SERVER['HTTP_ACCEPT'];
        $this->setHttpHeaders($requestContentType, $statusCode);

        return $statusCode;
    }

    function checkAddFollow($params){

        $utenteSearch = $this->getUtente(json_encode(json_decode($params)->utenteSearch->id));
        $utenteLog = $this->getUtente(json_encode(json_decode($params)->utenteLog->id));

        $utente = ['$oid'=>json_decode($params)->utenteSearch->id];

        if(empty($utenteSearch))
            return false;
        else
            foreach ($utenteLog[0]['utentiSeguiti'] as $value)
                if (json_encode($utente) == json_encode($value['idAutore']))
                    return false;

        return true;
    }


    function chkEmail($email)
    {
        // elimino spazi, "a capo" e altro alle estremità della stringa
        $email = trim($email);

        // se la stringa è vuota sicuramente non è una mail
        if(!$email) {
            return false;
        }

        // controllo che ci sia una sola @ nella stringa
        $num_at = count(explode( '@', $email )) - 1;
        if($num_at != 1) {
            return false;
        }

        // controllo la presenza di ulteriori caratteri "pericolosi":
        if(strpos($email,';') || strpos($email,',') || strpos($email,' ')) {
            return false;
        }

        // la stringa rispetta il formato classico di una mail?
        if(!preg_match( '/^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$/', $email)) {
            return false;
        }

        return true;
    }

    function chkSizeEmail($email){
        $flag=true;
        if(strlen($email)>50||strlen($email)<5){
            $flag=false;
        }

        return $flag;
    }


    public function checkPassword($pwd) {


        if (strlen($pwd) < 8 || strlen($pwd) > 32) {
            return false;
        }

        /*if (!preg_match("#[0-9]+#", $pwd)) {
            return false;
        }*/

        if (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$/", $pwd)) {
            return false;
        }

        return true;
    }

    public function checkUsername($nome){
        // se la stringa è vuota sicuramente non è una mail
        if(!$nome) {
            return false;
        }
        if(!preg_match( '/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/', $nome)) {
            return false;
        }
        if(strlen($nome)>25||strlen($nome)<5){
            return false;
        }
        return true;
    }

}