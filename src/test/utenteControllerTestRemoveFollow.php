<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 22/02/2019
 * Time: 16:25
 */

use PHPUnit\Framework\TestCase;
require_once("..\utente\utenteController.php");

class utenteControllerTestRemoveFollow extends TestCase
{

    public function testTC_RemUser_01()
    {
        $params = [
            'utenteLog' =>['id'=>'5c7526f7835bb31224000522'],
            'utenteSearch'=>['id'=>'5c752709835bb31e80004']
        ];

        $utenteController = new utenteController();

        $utenteSearch =['idAutore'=>['$oid'=>$params['utenteSearch']['id']]];

        $utente = $utenteController->removeFollow(json_encode($params));
        $this->assertStringNotContainsString(json_encode($utenteSearch), json_encode($utente[0]['utentiSeguiti']));

    }

    public function testTC_RemUser_02()
    {
        $params = [
            'utenteLog' =>['id'=>'5c7581e0835bb33bec007a42'],
            'utenteSearch'=>['id'=>'5c7581ef835bb30a48002772']
        ];

        $utenteController = new utenteController();

        $utenteSearch =['idAutore'=>['$oid'=>$params['utenteSearch']['id']]];

        $utente = $utenteController->removeFollow(json_encode($params));
        $this->assertStringNotContainsString(json_encode($utenteSearch), json_encode($utente[0]['utentiSeguiti']));

    }

}
