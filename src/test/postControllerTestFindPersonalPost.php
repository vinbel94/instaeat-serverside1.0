<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 24/02/2019
 * Time: 19:12
 */

use PHPUnit\Framework\TestCase;
require_once("../utente/utenteController.php");
require_once("../post/postController.php");

class postControllerTestFindPersonalPost extends TestCase
{
    public function testTC_findPersonalPost_01()
    {
        $autore = [
            'id'=>'5c7526f7835bb31224000522'
        ];

        $postController = new postController();
        $this->assertEquals(0, sizeof($postController->viewPost(json_encode($autore))));
    }

    public function testTC_findPersonalPost_02()
    {
        $autore = [
            'id'=>'5c7526f7835bb31224000522'
        ];

        $postController = new postController();
        $this->assertNotEmpty($postController->viewPost(json_encode($autore)));
    }

}
