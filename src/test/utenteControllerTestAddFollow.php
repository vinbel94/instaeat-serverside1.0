<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 22/02/2019
 * Time: 14:38
 */

use PHPUnit\Framework\TestCase;
require_once("..\utente\utenteController.php");

class utenteControllerTestAddFollow extends TestCase
{

    public function testTC_AddFollow_01()
    {
        $params = [
            'utenteLog' =>['id'=>'5c7581e0835bb33bec007a42'],
            'utenteSearch'=>['id'=>'5c752709835bb31e8000']
        ];

        $utenteController = new utenteController();
        $utente = $utenteController->addFollow(json_encode($params));

        $utenteSearch =['idAutore'=>['$oid'=>$params['utenteSearch']['id']]];
        $this->assertStringContainsString(json_encode($utenteSearch), json_encode($utente[0]['utentiSeguiti']));

    }

    public function testTC_AddFollow_02()//dovrebbe dare errore ma non lo da(bug che faremo vedere al prof)
    {
        $params = [
            'utenteLog' =>['id'=>'5c7581e0835bb33bec007a42'],
            'utenteSearch'=>['id'=>'5c7581ef835bb30a48002772']
        ];

        $utenteController = new utenteController();
        $utente = $utenteController->addFollow(json_encode($params));

        $utenteSearch =['idAutore'=>['$oid'=>$params['utenteSearch']['id']]];
        $this->assertStringContainsString(json_encode($utenteSearch), json_encode($utente[0]['utentiSeguiti']));

    }


    public function testTC_AddFollow_04()
    {
        $params = [
            'utenteLog' =>['id'=>'5c7581fd835bb3397c006392'],
            'utenteSearch'=>['id'=>'5c7581e0835bb33bec007a42']
        ];

        $utenteController = new utenteController();
        $utente = $utenteController->addFollow(json_encode($params));

        $utenteSearch =['idAutore'=>['$oid'=>$params['utenteSearch']['id']]];
        $this->assertStringContainsString(json_encode($utenteSearch), json_encode($utente[0]['utentiSeguiti']));

    }



}
