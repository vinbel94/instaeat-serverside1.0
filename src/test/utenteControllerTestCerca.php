<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 22/02/2019
 * Time: 16:28
 */

use PHPUnit\Framework\TestCase;
require_once("..\utente\utenteController.php");

class utenteControllerTestCerca extends TestCase
{

    public function testTC_Cname_01()
    {
        $utente = '5c7581ef835bb30a48002772';


        $utenteController = new utenteController();
        $listUtenti = $utenteController->getUtenti(json_encode($utente));
        $this->assertStringNotContainsString(json_encode($utente), json_encode($listUtenti));
    }
}
