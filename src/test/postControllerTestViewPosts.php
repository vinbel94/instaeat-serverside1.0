<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 22/02/2019
 * Time: 16:27
 */

use PHPUnit\Framework\TestCase;
require_once("../utente/utenteController.php");
require_once("../post/postController.php");

class postControllerTestViewPosts extends TestCase
{

    public function testTC_VisPostUser_01()
    {
        $autore = [
            'id'=>'5c719d67835bb31558003f82'
        ];

        $utenteController = new utenteController();
        $utente =$utenteController->getUtente(json_encode($autore['id']));

        $postController = new postController();
        $this->assertEquals($this->assertEmpty($utente[0]['utentiSeguiti']), $this->assertEmpty($postController->viewPost(json_encode($autore))));
    }

    public function testTC_VisPostUser_02()
    {
        $autore = [
            'id'=>'5c7526f7835bb31224000522'
        ];

        $utenteController = new utenteController();
        $utente =$utenteController->getUtente(json_encode($autore['id']));

        $postController = new postController();
        $this->assertEquals($this->assertNotEmpty($utente[0]['utentiSeguiti']), $this->assertNotEmpty($postController->viewPost(json_encode($autore))));
    }
}
