<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 24/02/2019
 * Time: 18:52
 */

use PHPUnit\Framework\TestCase;
require_once("..\utente\utenteController.php");

class utenteControllerTestGetUtente extends TestCase
{
    public function testTC_Cname_01()
    {
        $utente = '5c719d67835bb31558003f82';

        $utenteController = new utenteController();
        $this->assertNotEmpty($utenteController->getUtente(json_encode($utente)));
    }
}
