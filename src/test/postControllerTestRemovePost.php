<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 20/02/2019
 * Time: 17:17
 */

use PHPUnit\Framework\TestCase;
require_once("../post/postController.php");
class postControllerTestRemovePost extends TestCase
{

    public function testRemuvePost()
    {

        $post = [
            'id'=>"5c6d7e0c194620238c006b92",
            'titolo' => "La carbonara",
            'testo' => "piatto pronto in 10 minuti, porzione per 2 persone",
            'idAutore' => "5c6d78a01946204d70001152",
            'nomeAutore'=> "Mario",
            'path'=> "carbonara.png",
            'like'=>"5",
            'commenti'=>""

        ];
        $postController = new postController();
        $this->assertEquals($postController->remuvePost(json_encode($post)), true);


    }
}