<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 08/03/2019
 * Time: 18:45
 */

use PHPUnit\Framework\TestCase;
require_once("../post/postController.php");

class utenteControllerLasciaLikeTest extends TestCase
{

    public function testTC_Like_01()
    {
        $post = [
            'idPost' => "5c82c41b835bb32ce8007375",
            'utenteLog' => "5c7581fd835bb3397c006392",

        ];

        $postController = new postController();
        $this->assertEquals($postController->like(json_encode($post)), false);
    }

    public function testTC_Like_02()
    {
        $post = [
            'idPost' => "5c82c41b835bb32ce8007375",
            'utenteLog' => "5c7eb119835bb320a0006112",

        ];

        $postController = new postController();
        $this->assertEquals($postController->like(json_encode($post)), true);
    }

}
