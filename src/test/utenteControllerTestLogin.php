<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 20/02/2019
 * Time: 16:58
 */

use PHPUnit\Framework\TestCase;
require_once("../utente/utenteController.php");


class utenteControllerTestLogin extends TestCase
{

    /*PHPUnit supporta la condivisione del codice di installazione.
    Prima che venga eseguito un metodo di prova, viene invocato un metodo di modello chiamato setUp ().
    setUp () è dove crei gli oggetti contro cui testerai.
    Una volta che il metodo di prova ha terminato l'esecuzione,
    se è riuscito o meno, viene richiamato un altro metodo di modello chiamato tearDown ().
    tearDown () è dove si puliscono gli oggetti contro i quali si è verificato.*/


    public function testTC_Login_01()
    {
        //$this->setUp();
        $utente = [
            'nome' => "NonEsiste1",
            'email' => "salvatoreNonEsiste@gmail.com",
            'paswd' => "NonEsiste1"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->logIn(json_encode($utente))['flag'], true);


    }

    public function testTC_Login_02()
    {
        //$this->setUp();
        $utente = [
            'nome' => "Mario1",
            'email' => "MarioRossi@gmail.com",
            'paswd' => "Sbagliata1"
        ];

        $utenteController = new utenteController();

        $this->assertEquals($utenteController->logIn(json_encode($utente))['flag'], true);


    }


    public function testTC_Login_03()
    {
        //$this->setUp();
        $utente = [
            'nome' => "Mario1",
            'email' => "MarioRossi@gmail.com",
            'paswd' => "MarioRossi1"
        ];

        $utenteController = new utenteController();

        $this->assertEquals($utenteController->logIn(json_encode($utente))['flag'], true);


    }
}
