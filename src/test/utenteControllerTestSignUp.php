<?php
/**
 * Created by PhpStorm.
 * User: clara
 * Date: 20/02/2019
 * Time: 16:46
 */

use PHPUnit\Framework\TestCase;
require_once("..\utente\utenteController.php");

class utenteControllerTestSignUp extends TestCase
{

    public function testTC_SignUp_01()
    {
        $utente = [
            'nome' => "Ma",
            'email' => "Ma@gmail.com",
            'paswd' => "MarcoRossi1"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }

    public function testTC_SignUp_02()
    {
        $utente = [
            'nome' => "Marco",
            'email' => "MarcoRossi@gmail.com",
            'paswd' => "MarcoRossi1"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }

    public function testTC_SignUp_03()
    {
        $utente = [
            'nome' => "Marco1",
            'email' => "M",
            'paswd' => "MarcoRossi1"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }

    public function testTC_SignUp_04()
    {
        $utente = [
            'nome' => "MarcoRossi1",
            'email' => "Marcogmail.com",
            'paswd' => "MarcoRossi1"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }


    public function testTC_SignUp_05()
    {
        $utente = [
            'nome' => "MarcoRossi1",
            'email' => "MarcoRossi@gmail.com",
            'paswd' => "M"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }
    public function testTC_SignUp_06()
    {
        $utente = [
            'nome' => "MarcoRossi1",
            'email' => "Marcogmail.com",
            'paswd' => "marcorossi"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }

    public function testTC_SignUp_07()
    {
        $utente = [
            'nome' => "MarcoRossi1",
            'email' => "MarcoRossi@gmail.com",
            'paswd' => "MarcoRossi1"
        ];

        $utenteController = new utenteController();
        $this->assertEquals($utenteController->signUp(json_encode($utente)), true);

    }
}
