<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 10/02/2019
 * Time: 17:54
 */

require_once("../post/postDAO.php");
require_once("../db/dataBase.php");
require_once("../post/post.php");


class postCRUD implements postDAO
{
    public $db;


    public function __construct()
    {
        $this->db = new DataBase();
    }


    public function insertPost($post)
    {
        $insertOneResult = $this->db->getConnection()->posts->insertOne(['titolo' => $post->getTitolo(), 'testo' => $post->getTesto(), 'data' => date ("d/m/Y"), 'idAutore' => new \MongoDB\BSON\ObjectID($post->getAutore()), 'nomeAutore' => $post->getNomeAutore(),'path'=>$post->getPath(), 'like'=>[], 'commenti'=>[]]);

        return $insertOneResult->isAcknowledged();

    }

    public function findPost($query)
    {
        $collectionPosts = Array();
        //$aggregate = array(array('$lookup'=>array('from'=>'user','localField'=>'autore','foreignField'=>'_id','as'=>'infoAutore')), array('$match'=>array('autore'=>new \MongoDB\BSON\ObjectID($autore))));
        $cursor = $this->db->getConnection()->posts->aggregate($query);

        if(!$cursor->isDead()) {
            foreach ($cursor as $value) {
                $post = new post();
                $post->setId($value->_id);
                $post->setTitolo($value->titolo);
                $post->setTesto($value->testo);
                $post->setAutore($value->idAutore);
                $post->setNomeAutore($value->nomeAutore);
                $post->setData($value->data);
                $post->setPath($value->path);
                $post->setLike($value->like);
                array_push($collectionPosts, $post->getJson());
            }
            return $collectionPosts;

        }
        else
            return false;




    }

    public function updatePost($query, $update)
    {
        $updateOneResult = $this->db->getConnection()->posts->updateOne($query, $update);

        return $updateOneResult->isAcknowledged();

    }

    public function deletePost($post)
    {
        $deleteResult = $this->db->getConnection()->posts->deleteOne(['_id' =>new \MongoDB\BSON\ObjectID($post)]);

        return $deleteResult->isAcknowledged();

    }



}