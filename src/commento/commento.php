<?php
/**
 * Created by PhpStorm.
 * User: V.BELMONTE4
 * Date: 10/02/2019
 * Time: 18:16
 */




class commento
{
    private $idAutore;
    private $nomeAutore;
    private $testo;
    private $idPost;

    /**
     * commento constructor.
     */
    public function __construct(){}

    /**
     * @return mixed
     */
    public function getIdAutore()
    {
        return $this->idAutore;
    }

    /**
     * @param mixed $idAutore
     */
    public function setIdAutore($idAutore)
    {
        $this->idAutore = $idAutore;
    }

    /**
     * @return mixed
     */
    public function getTesto()
    {
        return $this->testo;
    }

    /**
     * @param mixed $testo
     */
    public function setTesto($testo)
    {
        $this->testo = $testo;
    }

    /**
     * @return mixed
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * @param mixed $idPost
     */
    public function setIdPost($idPost)
    {
        $this->idPost = $idPost;
    }

    /**
     * @return mixed
     */
    public function getNomeAutore()
    {
        return $this->nomeAutore;
    }

    /**
     * @param mixed $nomeAutore
     */
    public function setNomeAutore($nomeAutore)
    {
        $this->nomeAutore = $nomeAutore;
    }

    public function getJson(){

        return '{"nomeAutore" : "'.$this->getNomeAutore().'","testo":"'
            .$this->getTesto().'"}';
    }





}